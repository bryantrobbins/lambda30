# lambda30

These are scripts I used in the "How much Lambda can I Lambda in 30 minutes?" session
at DevSpace 2018 (Huntsville, AL).

I've cleaned them up a bit since that live demo. Feel free to use the scripts here as a reference,
but be aware I am making no claims regarding the production readiness of the samples included here.

Perhaps the most interesting/independently valuable thing here is the script in ```config/build.js```,
which implements some common scripts for working with Lambda and CloudFormation.

Cheers!

## Configuration

If you want to clone and experiment with this repo yourself, you should carry out the following
steps first.

### Install and configure the AWS CLI

The scripts depend on the AWS CLI being [installed](https://docs.aws.amazon.com/cli/latest/userguide/installing.html) 
and [configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) on your
machine.

Personally, I use the pip installer and environment variable credentials for development on all platforms.

### Create and configure an S3 bucket for packages

If you don't already have a bucket you'd like to use for storing your Lambda deployment packages,
you can create one using the CLI:

```
aws s3 mb s3://<some-bucket-name>
```

Now you can update the file ```config/build-config.js``` to point to your bucket. The file has the following
format:

```
module.exports = {
    bucket: '<your-bucket-name-here>',
    staged: ['package.json', 'package-lock.json', {local: 'src', rewrite: '.'} ]
}
```

If needed, you can update the ```staged``` configuration as well. This specifies which files should
be copied into the ```dist/``` directory, with support for changing the relative path (e.g., moving
```src``` to the top of the ```dist``` directory in the default configuration).

## NPM Scripts

Most scripts are using helpers implemented in the file ```config/build.js```.

### Install dependencies:

```
npm run install
# You may prefer "npm run ci" for a clean install from versions in package-lock.json
```

### Invoke a function from local source

```
npm run local <function-name> [ <input-json> ]
```

### Run build

```
npm run build
```

This creates a ```dist/``` folder of the source code of all functions as well as the ```node_modules``
installation of all production dependencies in package.json.

### Deploy a function to Lambda

```
npm run deploy -- <function-name>
```

This runs the build command, then uploads the ```dist``` folder using the 
```aws cloudformation package``` command, then uses the ```aws cloudformation deploy``` command to deploy the specific function specified.

### Invoke a deployed lambda function

```
npm run invoke -- <function-name> [ <input-json> ]
```

This uses the ```aws lambda invoke``` command to run the function.

## Running the Examples

### Hello World

```
[bryan@desktop-6 lambda30]$ npm run local hello

> lambda30@1.0.0 local /home/bryan/public-code/lambda30
> node config/build.js invokeLocal "hello"

Running build command invokeLocal
>>>>>INVOKE OUTPUT>>>>>
Hello lambda world!
>>>>>INVOKE OUTPUT>>>>>
[bryan@desktop-6 lambda30]$ npm run deploy hello

> lambda30@1.0.0 deploy /home/bryan/public-code/lambda30
> node config/build.js dist && node config/build.js deploy "hello"

Running build command dist
Creating dist directory at /home/bryan/public-code/lambda30/dist
added 14 packages in 1.525s
Running build command deploy
Uploading to 55e9658f9b97e252ddd5c5bc0cf74095  4874181 / 4874181.0  (100.00%)
Successfully packaged artifacts and wrote output template to file hello-packaged-20181014100955.yaml.
Execute the following command to deploy the packaged template
aws cloudformation deploy --template-file /home/bryan/public-code/lambda30/dist/hello-packaged-20181014100955.yaml --stack-name <YOUR STACK NAME>

Waiting for changeset to be created..
Waiting for stack create/update to complete
Successfully created/updated stack - lambda30-hello
[bryan@desktop-6 lambda30]$ npm run invoke hello

> lambda30@1.0.0 invoke /home/bryan/public-code/lambda30
> node config/build.js invoke "hello"

Running build command invoke
Output saved to hello-20181014101041-outfile
>>>>>INVOKE STATUS>>>>>
200
>>>>>INVOKE STATUS>>>>>
>>>>>INVOKE ERROR>>>>>
undefined
>>>>>INVOKE ERROR>>>>>
>>>>>INVOKE OUTPUT>>>>>
"Hello lambda world!"
>>>>>INVOKE OUTPUT>>>>>
>>>>>INVOKE LOGS>>>>>
START RequestId: 50ce7530-cfc3-11e8-a299-efb91e2c3a55 Version: $LATEST
END RequestId: 50ce7530-cfc3-11e8-a299-efb91e2c3a55
REPORT RequestId: 50ce7530-cfc3-11e8-a299-efb91e2c3a55  Duration: 4.24 ms       Billed Duration: 100 ms         Memory Size: 128 MB     Max Memory Used: 31 MB

>>>>>INVOKE LOGS>>>>>
```

### Hello Env (from GREETING environment variable)

Note: The local invoke takes variables from your local environment. The Lambda environment
variables are defined in the CloudFormation template at ```config/deploy/helloS3.yaml```.

```
[bryan@desktop-6 lambda30]$ GREETING="Bryan is a pretty nice guy." npm run local helloEnv

> lambda30@1.0.0 local /home/bryan/public-code/lambda30
> node config/build.js invokeLocal "helloEnv"

Running build command invokeLocal
>>>>>INVOKE OUTPUT>>>>>
Bryan is a pretty nice guy.
>>>>>INVOKE OUTPUT>>>>>
[bryan@desktop-6 lambda30]$ npm run deploy helloEnv

> lambda30@1.0.0 deploy /home/bryan/public-code/lambda30
> node config/build.js dist && node config/build.js deploy "helloEnv"

Running build command dist
Creating dist directory at /home/bryan/public-code/lambda30/dist
added 14 packages in 1.474s
Running build command deploy
Uploading to b36059e2ee9b6f58012238c77f27ac22  4874254 / 4874254.0  (100.00%)
Successfully packaged artifacts and wrote output template to file helloEnv-packaged-20181014101331.yaml.
Execute the following command to deploy the packaged template
aws cloudformation deploy --template-file /home/bryan/public-code/lambda30/dist/helloEnv-packaged-20181014101331.yaml --stack-name <YOUR STACK NAME>

Waiting for changeset to be created..
Waiting for stack create/update to complete
Successfully created/updated stack - lambda30-helloEnv
[bryan@desktop-6 lambda30]$ npm run invoke helloEnv

> lambda30@1.0.0 invoke /home/bryan/public-code/lambda30
> node config/build.js invoke "helloEnv"

Running build command invoke
Output saved to helloEnv-20181014101411-outfile
>>>>>INVOKE STATUS>>>>>
200
>>>>>INVOKE STATUS>>>>>
>>>>>INVOKE ERROR>>>>>
undefined
>>>>>INVOKE ERROR>>>>>
>>>>>INVOKE OUTPUT>>>>>
"Hello Lambda World, from the Environment this time!"
>>>>>INVOKE OUTPUT>>>>>
>>>>>INVOKE LOGS>>>>>
START RequestId: cdf88a8d-cfc3-11e8-906a-bb8239e295b6 Version: $LATEST
END RequestId: cdf88a8d-cfc3-11e8-906a-bb8239e295b6
REPORT RequestId: cdf88a8d-cfc3-11e8-906a-bb8239e295b6  Duration: 4.59 ms       Billed Duration: 100 ms         Memory Size: 128 MB     Max Memory Used: 31 MB

>>>>>INVOKE LOGS>>>>>
```

### Hello S3 (runs "s3 ls" command given bucket and prefix from input event)

Note: You'll have to update the input to point to your own bucket for this to work!

```
[bryan@desktop-6 lambda30]$ npm run local helloS3 ./test-input/bucket.json 

> lambda30@1.0.0 local /home/bryan/public-code/lambda30
> node config/build.js invokeLocal "helloS3" "./test-input/bucket.json"

Running build command invokeLocal
Sending event [object Object]
{ bucket: 'com.btr3.research', prefix: 'reports/' }
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-1.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-2.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-3.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-4.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-5.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-6.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-7.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-8.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_0.txt
reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_1.txt
>>>>>INVOKE OUTPUT>>>>>
[ 'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-1.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-2.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-3.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-4.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-5.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-6.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-7.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-8.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_0.txt',
  'reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_1.txt' ]
>>>>>INVOKE OUTPUT>>>>>
[bryan@desktop-6 lambda30]$ npm run deploy helloS3

> lambda30@1.0.0 deploy /home/bryan/public-code/lambda30
> node config/build.js dist && node config/build.js deploy "helloS3"

Running build command dist
Creating dist directory at /home/bryan/public-code/lambda30/dist
added 14 packages in 1.49s
Running build command deploy
Uploading to 0cefe664810c2512f1e565097aa0394d  4874241 / 4874241.0  (100.00%)
Successfully packaged artifacts and wrote output template to file helloS3-packaged-20181014101921.yaml.
Execute the following command to deploy the packaged template
aws cloudformation deploy --template-file /home/bryan/public-code/lambda30/dist/helloS3-packaged-20181014101921.yaml --stack-name <YOUR STACK NAME>

Waiting for changeset to be created..
Waiting for stack create/update to complete
Successfully created/updated stack - lambda30-helloS3
[bryan@desktop-6 lambda30]$ npm run invoke helloS3 ./test-input/bucket.json 

> lambda30@1.0.0 invoke /home/bryan/public-code/lambda30
> node config/build.js invoke "helloS3" "./test-input/bucket.json"

Running build command invoke
Output saved to helloS3-20181014102003-outfile
>>>>>INVOKE STATUS>>>>>
200
>>>>>INVOKE STATUS>>>>>
>>>>>INVOKE ERROR>>>>>
undefined
>>>>>INVOKE ERROR>>>>>
>>>>>INVOKE OUTPUT>>>>>
["reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-1.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-2.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-3.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-4.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-5.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-6.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-7.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-8.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_0.txt","reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_1.txt"]
>>>>>INVOKE OUTPUT>>>>>
>>>>>INVOKE LOGS>>>>>
START RequestId: a0279c2d-cfc4-11e8-8297-9d6ed83f5493 Version: $LATEST
2018-10-14T15:20:04.703Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    { bucket: 'com.btr3.research', prefix: 'reports/' }
2018-10-14T15:20:05.687Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-1.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-2.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-3.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-4.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-5.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-6.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-7.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_-8.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_0.txt
2018-10-14T15:20:05.706Z        a0279c2d-cfc4-11e8-8297-9d6ed83f5493    reports/amalga_ArgoUML_sq_l_1_testCaseFeatures_n_0_data_report_-1_1.txt
END RequestId: a0279c2d-cfc4-11e8-8297-9d6ed83f5493
REPORT RequestId: a0279c2d-cfc4-11e8-8297-9d6ed83f5493  Duration: 1024.38 ms    Billed Duration: 1100 ms        Memory Size: 128 MB     Max Memory Used: 34 MB

>>>>>INVOKE LOGS>>>>>
```

### Hello Dynamo (inserts given event into a DynamoDB table)

Note: You must create your own DynamoDB table and set its name in the TABLE environmenet variable
for local testing to work. Or, you can deploy the cloudformation template first
(using ```npm run deploy helloDynamo```) and find the created table's name in the DynamoDB console.

While it is possible to tell the CloudFormation template to use a hard-coded name for the table, I
generally avoid "named resources" in cloudformation due to the impacts of naming on resource
lifecycles. See [this help entry](https://aws.amazon.com/premiumsupport/knowledge-center/cloudformation-custom-name/)
for more info.

You'll also need to manually verify that the items in this example were actually inserted, as the logs
don't represent any definitive proof.

```
[bryan@desktop-6 lambda30]$ TABLE="lambda30-helloDynamo-HelloTable-46QP38VTA5CA" npm run local helloDynamo ./test-input/db.json 

> lambda30@1.0.0 local /home/bryan/public-code/lambda30
> node config/build.js invokeLocal "helloDynamo" "./test-input/db.json"

Running build command invokeLocal
Sending event [object Object]
{ name: 'what', value: 3 }
Writing item: {"id":{"S":"523e0aae-76ec-42fd-a538-4e7f98a9c70b"},"name":{"S":"what"},"value":{"N":"3"}}
>>>>>INVOKE OUTPUT>>>>>
undefined
>>>>>INVOKE OUTPUT>>>>>
[bryan@desktop-6 lambda30]$ npm run deploy helloDynamo

> lambda30@1.0.0 deploy /home/bryan/public-code/lambda30
> node config/build.js dist && node config/build.js deploy "helloDynamo"

Running build command dist
Creating dist directory at /home/bryan/public-code/lambda30/dist
added 14 packages in 1.515s
Running build command deploy
Uploading to 648336961fc3dc0552f1e2d93c23491e  4874362 / 4874362.0  (100.00%)
Successfully packaged artifacts and wrote output template to file helloDynamo-packaged-20181014103030.yaml.
Execute the following command to deploy the packaged template
aws cloudformation deploy --template-file /home/bryan/public-code/lambda30/dist/helloDynamo-packaged-20181014103030.yaml --stack-name <YOUR STACK NAME>

Waiting for changeset to be created..
Waiting for stack create/update to complete
Successfully created/updated stack - lambda30-helloDynamo
[bryan@desktop-6 lambda30]$ npm run invoke helloDynamo ./test-input/db.json 

> lambda30@1.0.0 invoke /home/bryan/public-code/lambda30
> node config/build.js invoke "helloDynamo" "./test-input/db.json"

Running build command invoke
Output saved to helloDynamo-20181014103125-outfile
>>>>>INVOKE STATUS>>>>>
200
>>>>>INVOKE STATUS>>>>>
>>>>>INVOKE ERROR>>>>>
undefined
>>>>>INVOKE ERROR>>>>>
>>>>>INVOKE OUTPUT>>>>>
null
>>>>>INVOKE OUTPUT>>>>>
>>>>>INVOKE LOGS>>>>>
START RequestId: 369b462b-cfc6-11e8-bcc8-bd354346ed6e Version: $LATEST
2018-10-14T15:31:26.744Z        369b462b-cfc6-11e8-bcc8-bd354346ed6e    { name: 'what', value: 3 }
2018-10-14T15:31:26.746Z        369b462b-cfc6-11e8-bcc8-bd354346ed6e    Writing item: {"id":{"S":"99cfccaf-1cf5-412f-8a5f-d7f56e65017c"},"name":{"S":"what"},"value":{"N":"3"}}
END RequestId: 369b462b-cfc6-11e8-bcc8-bd354346ed6e
REPORT RequestId: 369b462b-cfc6-11e8-bcc8-bd354346ed6e  Duration: 884.41 ms     Billed Duration: 900 ms         Memory Size: 128 MB     Max Memory Used: 34 MB

>>>>>INVOKE LOGS>>>>>
```