const hello = require('./hello');
const helloDebug = require('./helloDebug');
const helloEnv = require('./helloEnv');
const helloName = require('./helloName');
const helloS3 = require('./helloS3');
const helloDynamo = require('./helloDynamo');

module.exports = {
    hello,
    helloDebug,
    helloEnv,
    helloName,
    helloS3,
    helloDynamo
}
