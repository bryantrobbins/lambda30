async function hello(event, context, callback) {
    // Return a value from the lambda
    callback(null, 'Hello lambda world!');
}

module.exports = hello;