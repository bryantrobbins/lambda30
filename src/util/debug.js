var debug = require('debug');

module.exports = function(arg) {
    debug.log = console.log.bind(console);
    return debug(arg);
};
