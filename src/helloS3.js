var AWS = require('aws-sdk');
var s3 = new AWS.S3();

async function helloS3(event, context, callback) {
    // Check input
    console.log(event);

    // Make a call to S3
    var results = await s3.listObjectsV2({
        Bucket: `${event.bucket}`,
        Prefix: `${event.prefix}`,
        MaxKeys: 10
    }).promise();

    var keysOnly = results.Contents.map(obj => {
        console.log(`${obj.Key}`);
        return obj.Key;
    });

    callback(null, keysOnly);
}

module.exports = helloS3;