var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB();
var uuid = require('uuid/v4');

const reducer = function(ev) {
    return function(a,i) {
        var sval = ev[i].toString();
        a[i] = isNaN(ev[i])
            ? {S: sval}
            : {N: sval};
        return a;
    };
};

async function helloDynamo(event, context, callback) {
    // Get table name from ENV
    var tableName = process.env.TABLE;

    // Build an item for insert
    // Include all keys from event + id (required)
    console.log(event);
    var item = Object.keys(event).reduce(
        reducer(event),
        {
            id: {
                S: `${uuid()}`
            }
        });
    console.log(`Writing item: ${JSON.stringify(item)}`)

    await dynamodb.putItem({
        Item: item,
        TableName: tableName
    }).promise();

    callback(null);
}

module.exports = helloDynamo;