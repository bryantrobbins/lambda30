async function helloEnv(event, context, callback) {
    // Return a value from the lambda
    callback(null, `${process.env.GREETING}`);
}

module.exports = helloEnv;