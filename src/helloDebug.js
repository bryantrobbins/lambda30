var debug = require('./util/debug')('lambda30:helloDebug');

async function helloDebug(event, context, callback) {
    console.log(`DEBUG is: ${process.env.DEBUG}`);
    debug("I'm using debug in a lambda");
    // Return a value from the lambda
    callback(null, 'Hello lambda world!');
}

module.exports = helloDebug;