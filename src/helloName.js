async function helloName(event, context, callback) {
    // Return a value from the lambda
    callback(null, `Hello ${event.name}`);
}

module.exports = helloName;